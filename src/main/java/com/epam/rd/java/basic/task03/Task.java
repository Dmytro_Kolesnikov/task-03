package com.epam.rd.java.basic.task03;

public class Task {

	private static final int N = 3;
	
	public static void main(String[] args) {
		Parking p = new Parking(N);

		System.out.println(p);
		
		p.arrive(1);
		System.out.println(p);

		p.arrive(1);
		System.out.println(p);

		p.arrive(1);
		System.out.println(p);
	}

}